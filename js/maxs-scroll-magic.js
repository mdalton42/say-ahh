let ticking = false;

let lastScrollY = window.scrollY;
let intViewportHeight = window.innerHeight;

let pagePercent = () => lastScrollY / intViewportHeight;

const throttledScroll = new CustomEvent("throttledScroll", {
  detail: { page: pagePercent },
});
const throttledResize = new CustomEvent("throttledResize");

const onScroll = (e) => {
  // Store the scroll value for laterz.
  lastScrollY = window.scrollY;

  if (!ticking) {
    requestAnimationFrame(() => {
      window.dispatchEvent(throttledScroll);
      ticking = false;
    });

    ticking = true;
  }
};

let resizing;
const onResize = (e) => {
  intViewportHeight = window.innerHeight;
  clearTimeout(resizing);
  resizing = setTimeout(() => {
    requestAnimationFrame(() => {
      window.dispatchEvent(throttledResize);
    });
  }, 100);
};

const clampNumber = (num, a, b) =>
  Math.max(Math.min(num, Math.max(a, b)), Math.min(a, b));

window.addEventListener("scroll", onScroll);
window.onload = () => {
  onScroll();
  onResize();
};
window.addEventListener("resize", onResize);

export const scrollStyles = ({ properties = {}, styles }, callback) => {
  let computedStyleElements = styles.map((style, index) =>
    styleReducer(style, index, properties)
  );

  resizeListener(() => {
    computedStyleElements = styles.map((style, index) =>
      styleReducer(style, index, properties)
    );
  });

  window.addEventListener("throttledScroll", (e) => {
    let calculatedOps = computedStyleElements.reduce(
      (
        acc,
        {
          pageRange,
          valueRange,
          name,
          rounding,
          element,
          styleString,
          anchor,
          styleName,
          vendorString,
          anchorHeight,
        }
      ) => {
        const [pageStart, pageEnd] = pageRange;
        let relativePage = anchor
          ? getCoords(anchor, anchorHeight)
          : e.detail.page() - pageStart;
        let value = valueRange.map(([valueStart, valueEnd]) => {
          let ratio = (pageEnd - pageStart) / (valueEnd - valueStart);
          return clampNumber(
            valueStart + relativePage / ratio,
            valueStart,
            valueEnd
          );
        });
        if (value.length === 1) value = value[0];
        if (element && styleString && styleName) {
          if (vendorString) element.style[vendorString] = styleString(value);
          element.style[styleName] = styleString(value);
        }
        return {
          ...acc,
          [name]: Math.round(value * rounding) / rounding,
        };
      },
      {}
    );
    if (callback) callback(calculatedOps);
  });
};

const getCoords = (element, height) => {
  return -(element.getBoundingClientRect().top / height);
};

const styleReducer = (style, index, globals) => {
  globals.rounding = globals.rounding || 1000;
  const newStyle = { ...style };

  for (const [key, value] of Object.entries(globals)) {
    newStyle[key] = newStyle[key] || value;
  }

  const { anchor, valueRange, styleName, element } = newStyle;

  newStyle.name = name || index;
  if (anchor && isNaN(anchor)) newStyle.anchorHeight = anchor.clientHeight;
  newStyle.valueRange = Array.isArray(valueRange[0])
    ? valueRange
    : [valueRange];

  let uppercased = styleName.charAt(0).toUpperCase() + styleName.slice(1);
  if (element && element.style[`webkit${uppercased}`] !== undefined)
    newStyle.vendorString = `webkit${uppercased}`;

  return newStyle;
};

export const pageListener = (callback) => {
  window.addEventListener("throttledScroll", (e) => {
    callback(e.detail.page(), lastScrollY);
  });
};

export const resizeListener = (callback) => {
  window.addEventListener("throttledResize", (e) => {
    callback();
  });
};
