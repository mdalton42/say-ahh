import { scrollStyles } from "./maxs-scroll-magic";
import { Testimonials } from "../testimonials.json";

const navBar = document.getElementById("nav");
const logo = document.getElementById("logo");
const logoText = document.getElementById("logo-text");
const logoWrapper = document.getElementById("logo-wrapper");
const aboutRow = document.getElementById("about-row");
const stickyBackground = document.getElementById("sticky-background");
const target = document.getElementById("testis");

let intersectingTestimonialRow = false;
let options = { threshold: 0.8 };
let callback = (entries, observer) => {
  entries.forEach(({ isIntersecting }) => {
    intersectingTestimonialRow = isIntersecting;
  });
};
let observer = new IntersectionObserver(callback, options);

observer.observe(target);

setInterval(() => {
  if (intersectingTestimonialRow) nextTestimonial();
}, 10000);

const testimonialElement = document.querySelector(".testimonial");
const testimonialText = testimonialElement.querySelector(".text");
const testimonialPerson = testimonialElement.querySelector(".reviewer");
let currentTestimonial = 0;
const nextTestimonial = () => {
  testimonialElement.classList.add("animating");
  setTimeout(() => {
    testimonialElement.classList.remove("animating");
    let { name, text } = Testimonials[currentTestimonial];
    testimonialText.innerHTML = text;
    testimonialPerson.innerHTML = name;
    currentTestimonial = (currentTestimonial + 1) % Testimonials.length;
  }, 300);
};

const scrollStyleObject = {
  properties: {
    pageRange: [0, 0.3],
  },
  styles: [
    {
      valueRange: [0, 0.5],
      styleName: "backgroundColor",
      element: navBar,
      styleString: (value) => `rgba(0,0,0,${value})`,
    },
    {
      valueRange: [
        [0, 20],
        [100, 180],
      ],
      styleName: "backdropFilter",
      styleString: (value) => `blur(${value[0]}px) saturate(${value[1]}%)`,
      element: navBar,
    },
    {
      valueRange: [150, 60],
      styleName: "height",
      element: logo,
      styleString: (value) => `${value}px`,
    },
    {
      pageRange: [0, 0.2],
      valueRange: [1, 0],
      styleName: "opacity",
      element: logoText,
      styleString: (value) => `${value}`,
    },
    {
      valueRange: [
        [0, 0.2],
        [0, 40],
      ],
      styleName: "transform",
      element: logoText,
      styleString: (value) => `translate(-50%, -${value}px)`,
    },
    {
      valueRange: [60, 15],
      styleName: "paddingBottom",
      element: logoWrapper,
      styleString: (value) => `${value}px`,
    },
    {
      valueRange: [
        [0, 30],
        [100, 120],
      ],
      styleName: "filter",
      styleString: (value) => `blur(${value[0]}px) saturate(${value[1]}%)`,
      element: stickyBackground,
      anchor: aboutRow,
    },
    {
      valueRange: [1, 0.6],
      styleName: "opacity",
      styleString: (value) => `${value}`,
      element: stickyBackground,
      anchor: aboutRow,
    },
  ],
};

scrollStyles(scrollStyleObject);
